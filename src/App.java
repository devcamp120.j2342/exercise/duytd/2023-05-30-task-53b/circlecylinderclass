import com.devcamp.models.Circle;
import com.devcamp.models.Cylinder;

public class App {
    public static void main(String[] args) throws Exception {
        Circle circle1 = new Circle();
        System.out.println("Circle 1");
        System.out.println(circle1);
        //System.out.println(circle1);

        Circle circle2 = new Circle(5);
        System.out.println("Circle 2");
        System.out.println(circle2);

        Circle circle3 = new Circle(7, "blue");
        System.out.println("Circle 3");
        System.out.println(circle3);
        
        Cylinder cylinder1 = new Cylinder();
        System.out.println("cylinder 1");
        System.out.println(cylinder1);

        Cylinder cylinder2 = new Cylinder(5.5);
        System.out.println("cylinder 2");
        System.out.println(cylinder2);

        Cylinder cylinder3 = new Cylinder(3.5,2.5);
        System.out.println("cylinder 3");
        System.out.println(cylinder3);

        Cylinder cylinder4 = new Cylinder(3.5, "green",2.5);
        System.out.println("cylinder 4");
        System.out.println(cylinder4);
    }
}
